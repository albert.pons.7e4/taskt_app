package com.example.tasktapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import android.view.animation.AlphaAnimation
import com.example.tasktapp.R
import com.example.tasktapp.data.model.TaskList

class ListsAdapter(private val taskLists: List<TaskList>, private val listener: OnItemClickListener) : RecyclerView.Adapter<ListsAdapter.ListOfListsViewHolder>(){

    private val FADE_DURATION = 1000 //FADE_DURATION in milliseconds

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListOfListsViewHolder {
        // Estem creant la vista del layout del item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task,parent,false)

        // I retornem un FiewListViewHolder que gestionara aquesta vista
        return ListOfListsViewHolder(view)
    }

    override fun onBindViewHolder(holderOfLists: ListOfListsViewHolder, position: Int) {
        // Cridarem al holder (FilmListViewHolder) pasanli per parametre un item de la llista
        holderOfLists.bindData(taskLists[position])

        setFadeAnimation(holderOfLists.itemView)
    }

    override fun getItemCount(): Int {
        // Retorno la mida de la llista
        return taskLists.size
    }

    // Aquesta clase representa una fila concreta del RecycleView i s'encarrega de dibuixar aquesta fila
    inner class ListOfListsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var listName: TextView
        private var elementsNumber: TextView

        init {
            elementsNumber = itemView.findViewById(R.id.elements_number)
            listName = itemView.findViewById(R.id.list_name)
        }

        // Funcio que servira per assignar la informacio d'un item de la llista a la vista que el representa
        fun bindData(taskList: TaskList){
            listName.text = taskList.nomLlista.toUpperCase()
            elementsNumber.text = "Elements in list: ".plus(taskList.taskItemsList.size)

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }

    private fun setFadeAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = FADE_DURATION.toLong()
        view.startAnimation(anim)
    }

}