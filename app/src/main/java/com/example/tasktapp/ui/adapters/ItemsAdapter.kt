package com.example.tasktapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tasktapp.R
import com.example.tasktapp.data.api.APIService
import com.example.tasktapp.data.model.TaskItem
import com.example.tasktapp.data.repository.Repository
import com.example.tasktapp.ui.viewmodels.TasktViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemsAdapter (private val taskItems: List<TaskItem>, private val listener: OnItemClickListener, private val idUser: Int) : RecyclerView.Adapter<ItemsAdapter.ListOfItemsViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListOfItemsViewHolder {
        // Estem creant la vista del layout del item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_item,parent,false)

        // I retornem un FiewListViewHolder que gestionara aquesta vista
        return ListOfItemsViewHolder(view)
    }

    override fun onBindViewHolder(holderOfLists: ListOfItemsViewHolder, position: Int) {
        // Cridarem al holder (FilmListViewHolder) pasanli per parametre un item de la llista
        holderOfLists.bindData(taskItems[position],idUser)

        setFadeAnimation(holderOfLists.itemView)
    }

    override fun getItemCount(): Int {
        // Retorno la mida de la llista
        return taskItems.size
    }

    // Aquesta clase representa una fila concreta del RecycleView i s'encarrega de dibuixar aquesta fila
    inner class ListOfItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var itemName: TextView = itemView.findViewById(R.id.item_name)
        private var checkBox: CheckBox = itemView.findViewById(R.id.checkbox)

        // Funcio que servira per assignar la informacio d'un taskItem de la llista a la vista que el representa
        fun bindData(taskItem: TaskItem, idUser: Int){
            itemName.text = taskItem.nomItem
            checkBox.isChecked = taskItem.isChecked

            itemView.setOnClickListener(this)

            checkBox.setOnClickListener {
                taskItem.isChecked = !taskItem.isChecked
                checkOneItem(idUser, taskItem.idLlista,taskItem.idItem!!)
            }
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }

    private fun setFadeAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 1000
        view.startAnimation(anim)
    }

    fun checkOneItem(idUser: Int, listId: Int, itemId: Int){
        val repository = Repository()
        val call = repository.checkItem(idUser, listId,itemId)

        call.enqueue(object : Callback<TaskItem> {
            override fun onResponse(call: Call<TaskItem>, response: Response<TaskItem>) {
            }
            override fun onFailure(call: Call<TaskItem>, t: Throwable) {
            }
        })
    }
}