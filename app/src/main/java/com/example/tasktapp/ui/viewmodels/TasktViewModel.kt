package com.example.tasktapp.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.example.tasktapp.data.model.TaskItem
import com.example.tasktapp.data.model.TaskList
import com.example.tasktapp.data.model.User
import com.example.tasktapp.data.repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class TasktViewModel: ViewModel() {
    private val repository = Repository()
    private var tasksList = mutableListOf<TaskList>()
    private var tasksItemsList = mutableListOf<TaskItem>()
    var idUser = 0
    var can = false
    var users = mutableListOf<User>()
    var exists = false

    fun getUsers(){
        val call = repository.getUsers()
        call.enqueue(object : Callback<MutableList<User>> {
            override fun onResponse(call: Call<MutableList<User>>, response: Response<MutableList<User>>) {
                if (response.isSuccessful){
                    users.clear()
                    users.addAll(response.body()!!)
                }
            }
            override fun onFailure(call: Call<MutableList<User>>, t: Throwable) {
            }
        })

    }

    fun register(username: String, password: String){
        exists = false
        for(i in users){
            if(i.username == username){
                exists = true
            }
        }

        if (!exists) {
            val cryptpass = Base64.getEncoder().encodeToString(password.toByteArray())
            val call = repository.register(username, password)

            call.enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>, response: Response<User>) {
                    getUsers()
                }

                override fun onFailure(call: Call<User>, t: Throwable) {
                }
            })
        }
    }

    fun login(username: String, password: String){
        for (i in users){
            if (i.username == username && i.password == password){
                can = true
                idUser = i.idUsuari!!
                loadLists()
            }
        }
    }

    fun getLists(): MutableList<TaskList> {
        loadLists()
        return tasksList
    }
    fun getItems(position: Int): MutableList<TaskItem> {
        loadItems(position)
        return tasksItemsList
    }
    fun loadLists(){
        val call = repository.getLists(idUser)

        call.enqueue(object : Callback<MutableList<TaskList>> {
            override fun onResponse(call: Call<MutableList<TaskList>>, response: Response<MutableList<TaskList>>) {
                if (response.isSuccessful){
                    tasksList.clear()
                    tasksList.addAll(response.body()!!)
                }
            }
            override fun onFailure(call: Call<MutableList<TaskList>>, t: Throwable) {
                //TODO("Not yet implemented")
            }
        })
    }
    fun loadItems(position: Int) {
        val call = repository.getItemsOfList(idUser, tasksList[position].idLlista!!)

        call.enqueue(object : Callback<MutableList<TaskItem>> {
            override fun onResponse(call: Call<MutableList<TaskItem>>, response: Response<MutableList<TaskItem>>) {
                tasksItemsList.clear()
                tasksItemsList.addAll(response.body()!!)
            }
            override fun onFailure(call: Call<MutableList<TaskItem>>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
    fun addOneList(nomLlista: String){
        val call = repository.addList(idUser, TaskList(null,nomLlista, mutableListOf<TaskItem>()))

        call.enqueue(object : Callback<TaskList> {
            override fun onResponse(call: Call<TaskList>, response: Response<TaskList>) {
                if (response.isSuccessful){
                    loadLists()
                }
            }
            override fun onFailure(call: Call<TaskList>, t: Throwable) {
            }
        })
    }
    fun addOneItem(position: Int, nomItem: String){
        val call = repository.addItem(idUser, tasksList[position].idLlista!!,
            TaskItem(null,tasksList[position].idLlista!!,nomItem,0,false)
        )

        call.enqueue(object : Callback<TaskItem> {
            override fun onResponse(call: Call<TaskItem>, response: Response<TaskItem>) {
                if (response.isSuccessful){
                    loadItems(position)
                }
            }
            override fun onFailure(call: Call<TaskItem>, t: Throwable) {
            }
        })
    }
    fun deleteOneList(position: Int){
        val call = repository.deleteList(idUser, tasksList[position].idLlista!!)

        call.enqueue(object : Callback<TaskList> {
            override fun onResponse(call: Call<TaskList>, response: Response<TaskList>) {
                if (response.isSuccessful){
                    tasksList.removeAt(position)
                    tasksItemsList.clear()
                }
            }
            override fun onFailure(call: Call<TaskList>, t: Throwable) {
            }
        })
    }

    fun deleteOneItem(position: Int, itemPos: Int){
        val call = repository.deleteItem(idUser, tasksList[position].idLlista!!, tasksItemsList[itemPos].idItem!!)

        call.enqueue(object : Callback<TaskItem> {
            override fun onResponse(call: Call<TaskItem>, response: Response<TaskItem>) {
                if (response.isSuccessful){
                    tasksItemsList.removeAt(position)
                }
            }
            override fun onFailure(call: Call<TaskItem>, t: Throwable) {
            }
        })
    }
}