package com.example.tasktapp.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.transition.TransitionInflater
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.tasktapp.R
import com.example.tasktapp.SwipeToDelete
import com.example.tasktapp.ui.viewmodels.TasktViewModel
import com.example.tasktapp.ui.adapters.ListsAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.log

class ListsFragment : Fragment(R.layout.tasks_fragment), ListsAdapter.OnItemClickListener {
    private  lateinit var recyclerView: RecyclerView
    private val viewModel: TasktViewModel by activityViewModels()
    private lateinit var newListButton: FloatingActionButton
    private lateinit var listName: TextInputLayout
    private lateinit var infoButton: ImageView
    private lateinit var swipeLayout: SwipeRefreshLayout
    private lateinit var logout: ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.fade)

        logout = view.findViewById(R.id.backArrow)

        logout.setOnClickListener{
            val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                R.style.MaterialAlertDialog_rounded
            ).
            setMessage(resources.getString(R.string.logout))
                .setTitle("LogOut")
                .setCancelable(false)
                .setNegativeButton("CANCEL"){ _, _->}
                .setPositiveButton("OK") { _, _ ->
                    viewModel.idUser = 0
                    viewModel.can = false
                    val action = ListsFragmentDirections.actionListsFragmentToLoginFragment()
                    findNavController().navigate(action)
                }.create()
            dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialogBuilder.show()
        }

        recyclerView = view.findViewById(R.id.recycler_view)

        // Definim com estaran ordenats els elements de la llista (pot ser Linear, Grid o Custom)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        // Assignem un adapter al RecyclerView pasant per paramentre la llista d'elements
        val adapter = ListsAdapter(viewModel.getLists(),this)

        Handler().postDelayed({
            recyclerView.adapter = adapter }, 1000)

        val swipeToDelete = object : SwipeToDelete(view.context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val pos = viewHolder.adapterPosition
                val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                    R.style.MaterialAlertDialog_rounded
                ).setMessage("¡WATCH OUT! ¿ARE YOU SURE YOU WANT TO DELETE THIS ELEMENT? IF SO PRESS DELETE")
                    .setTitle("WARNING")
                    .setCancelable(false)
                    .setNegativeButton("CANCEL") {_,_ ->
                        adapter.notifyItemChanged(pos)
                    }
                    .setPositiveButton("DELETE") { _, _ ->
                        viewModel.deleteOneList(pos)
                        adapter.notifyItemChanged(pos)
                        setFadeOutAnimation(recyclerView)
                        Handler().postDelayed({
                            recyclerView.adapter = adapter
                        }, 800)
                    }.create()
                dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
                dialogBuilder.show()
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeToDelete)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        newListButton = view.findViewById(R.id.button)

        newListButton.setOnClickListener{
            val builder = layoutInflater.inflate(R.layout.custom_dialog, null)
            val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                R.style.MaterialAlertDialog_rounded
            ).setView(builder)
                .setCancelable(false).setNegativeButton("CANCEL"){_,_ ->}.setPositiveButton("CREATE"){ _, _ ->
                    listName = builder.findViewById(R.id.nameInputLayout)
                    viewModel.addOneList(listName.editText?.text.toString())
                    setFadeOutAnimation(recyclerView)
                    Handler().postDelayed({
                        recyclerView.adapter = adapter
                    }, 1000)
                }.create()
            dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialogBuilder.show()
        }

        infoButton = view.findViewById(R.id.infoButton)

        infoButton.setOnClickListener {
            val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                R.style.MaterialAlertDialog_rounded
            ).
            setMessage(resources.getString(R.string.info))
                .setTitle("HELP")
                .setCancelable(false)
                .setPositiveButton("OK") { _, _ ->
                }.create()
            dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialogBuilder.show()
        }

        swipeLayout = view.findViewById(R.id.swiperefresh)
        // Adding Listener
        // Adding Listener
        swipeLayout.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code here
            Handler().postDelayed({ // Stop animation (This will be after 3 seconds)
                val action = ListsFragmentDirections.actionListsFragmentSelf()
                findNavController().navigate(action)
            }, 1000) // Delay in millis
            // To keep animation for 4 seconds
            Handler().postDelayed({ // Stop animation (This will be after 3 seconds)
                swipeLayout.isRefreshing = false
            }, 1000) // Delay in millis
        })
    }

    override fun onItemClick(position: Int) {
        val action = ListsFragmentDirections.actionListsFragmentToItemsFragment(position)
        findNavController().navigate(action)
    }

    private fun setFadeOutAnimation(view: View) {
        val anim = AlphaAnimation(1.0f, 0.0f)
        anim.duration = 1000
        view.startAnimation(anim)
    }
}
