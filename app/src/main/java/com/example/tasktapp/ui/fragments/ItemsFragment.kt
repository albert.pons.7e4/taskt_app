package com.example.tasktapp.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.transition.TransitionInflater
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.tasktapp.R
import com.example.tasktapp.SwipeToDelete
import com.example.tasktapp.ui.viewmodels.TasktViewModel
import com.example.tasktapp.ui.adapters.ItemsAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout

class ItemsFragment : Fragment(R.layout.task_items_fragment), ItemsAdapter.OnItemClickListener {
    private  lateinit var recyclerView: RecyclerView
    private val viewModel: TasktViewModel by activityViewModels()
    private lateinit var newItemButon: FloatingActionButton
    private lateinit var listName: TextView
    private lateinit var goBackButton: ImageView
    private lateinit var itemName: TextInputLayout
    private lateinit var swipeLayout: SwipeRefreshLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.fade)

        recyclerView = view.findViewById(R.id.recycler_view)

        // Definim com estaran ordenats els elements de la llista (pot ser Linear, Grid o Custom)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        // Assignem un adapter al RecyclerView pasant per paramentre la llista d'elements
        val position = arguments?.getInt("position")
        var adapter = ItemsAdapter(viewModel.getItems(position!!),this, viewModel.idUser)
        Handler().postDelayed({
            recyclerView.adapter = adapter
        }, 1000)
        goBackButton = view.findViewById(R.id.backArrow)

        goBackButton.setOnClickListener{
            viewModel.loadLists()
            Handler().postDelayed({
                Navigation.findNavController(view).navigate(R.id.action_itemsFragment_to_listsFragment)
            }, 400)
        }

        val swipeToDelete = object : SwipeToDelete(view.context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val pos = viewHolder.adapterPosition
                val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                    R.style.MaterialAlertDialog_rounded
                ).setMessage("¡WATCH OUT! ¿ARE YOU SURE YOU WANT TO DELETE THIS ELEMENT? IF SO PRESS DELETE")
                    .setTitle("WARNING")
                    .setCancelable(false)
                    .setNegativeButton("CANCEL") {_,_ ->
                        adapter.notifyItemChanged(pos)
                    }
                    .setPositiveButton("DELETE") { _, _ ->
                        viewModel.deleteOneItem(position, pos)
                        adapter.notifyItemChanged(pos)
                        setFadeOutAnimation(recyclerView)
                        adapter = ItemsAdapter(viewModel.getItems(position),this@ItemsFragment, viewModel.idUser)
                        Handler().postDelayed({
                            recyclerView.adapter = adapter
                        }, 800)
                    }.create()
                dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
                dialogBuilder.show()
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeToDelete)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        listName = view.findViewById(R.id.name)
        newItemButon = view.findViewById(R.id.button)

        listName.text = viewModel.getLists()[position].nomLlista

        newItemButon.setOnClickListener{
            val builder = layoutInflater.inflate(R.layout.custom_dialog2, null)
            val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                R.style.MaterialAlertDialog_rounded
            ).setView(builder)
                .setCancelable(false).setNegativeButton("CANCEL"){_,_ ->}.setPositiveButton("CREATE"){ _, _ ->
                    itemName = builder.findViewById(R.id.nameInputLayout)
                    viewModel.addOneItem(position,itemName.editText?.text.toString())
                    setFadeOutAnimation(recyclerView)
                    adapter = ItemsAdapter(viewModel.getItems(position),this, viewModel.idUser)
                    Handler().postDelayed({
                        recyclerView.adapter = adapter
                    }, 1000)
                }.create()
            dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
            dialogBuilder.show()
        }

        swipeLayout = view.findViewById(R.id.swiperefresh2)
        // Adding Listener
        // Adding Listener
        swipeLayout.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code here
            Handler().postDelayed({ // Stop animation (This will be after 3 seconds)
                val action = ItemsFragmentDirections.actionItemsFragmentSelf(position)
                findNavController().navigate(action)
            }, 1000) // Delay in millis
            // To keep animation for 4 seconds
            Handler().postDelayed({ // Stop animation (This will be after 3 seconds)
                swipeLayout.isRefreshing = false
            }, 1000) // Delay in millis
        })
    }

    override fun onItemClick(position: Int) {
    }

    private fun setFadeOutAnimation(view: View) {
        val anim = AlphaAnimation(1.0f, 0.0f)
        anim.duration = 1000
        view.startAnimation(anim)
    }

    private fun setFadeInAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 1000
        view.startAnimation(anim)
    }

}
