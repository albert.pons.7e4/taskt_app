package com.example.tasktapp.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.transition.TransitionInflater
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.tasktapp.R
import com.example.tasktapp.ui.viewmodels.TasktViewModel
import com.google.android.material.textfield.TextInputEditText

class LoginFragment : Fragment(R.layout.login_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var register: TextView
    lateinit var login: Button

    private val viewModel: TasktViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.slide_right)

        viewModel.getUsers()
        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)
        register = view.findViewById(R.id.register)
        login = view.findViewById(R.id.login)


        register.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.registerFragment)
        }

        login.setOnClickListener{
            viewModel.login(username.text.toString(), password.text.toString())

            Handler().postDelayed({
                if (viewModel.can) {
                    Navigation.findNavController(view)
                        .navigate(R.id.action_loginFragment_to_listsFragment)
                }else{
                    password.error = getString(R.string.pswd_error)
                    password.setText("")
                }
            }, 800)
        }
    }
}