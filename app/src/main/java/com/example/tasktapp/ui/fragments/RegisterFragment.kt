package com.example.tasktapp.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.transition.TransitionInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.tasktapp.R
import com.example.tasktapp.ui.viewmodels.TasktViewModel
import com.google.android.material.textfield.TextInputEditText

class RegisterFragment : Fragment(R.layout.register_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var backArrow: ImageView
    lateinit var login: Button

    private val viewModel: TasktViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.slide_left)


        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)
        backArrow = view.findViewById(R.id.backArrow)
        login = view.findViewById(R.id.login)


        backArrow.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.loginFragment)
        }

        login.setOnClickListener{
            viewModel.register(username.text.toString(),password.text.toString())

            Handler().postDelayed({
                if (viewModel.exists){
                    Toast.makeText(context, "Este nombre de usuario ya existe, prueba con otro", Toast.LENGTH_SHORT).show()

                }else{
                    Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment)
                    Toast.makeText(context, "¡Usuario creado correctamente!", Toast.LENGTH_SHORT).show()
                }
            }, 800)

        }
    }
}