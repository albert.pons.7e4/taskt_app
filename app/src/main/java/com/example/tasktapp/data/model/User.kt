package com.example.tasktapp.data.model

import com.google.gson.annotations.SerializedName

data class User (@SerializedName("idUsuari") var idUsuari: Int?,
                 @SerializedName("nomUsuari") var username: String?,
                 @SerializedName("password") var password: String?,
                 @SerializedName("llistaItem") var lists: MutableList<TaskList>?
        )