package com.example.tasktapp.data.model

import com.google.gson.annotations.SerializedName

data class TaskList(@SerializedName("idLlista") var idLlista: Int?, @SerializedName("nomLlista") var nomLlista: String, @SerializedName("llistaItem") var taskItemsList: MutableList<TaskItem>)