package com.example.tasktapp.data.api

import com.example.tasktapp.data.model.TaskItem
import com.example.tasktapp.data.model.TaskList
import com.example.tasktapp.data.model.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface APIService {
    @GET("todousers")
    fun getUsers(): Call<MutableList<User>>

    @POST("todousers")
    fun register(@Body user: User): Call<User>

    @GET("todousers/{idUser}/todolists")
    fun getLists(@Path("idUser") idUser: Int): Call<MutableList<TaskList>>

    @POST("todousers/{idUser}/todolists")
    fun addList(@Path("idUser") idUser: Int, @Body taskList: TaskList): Call<TaskList>

    @DELETE("todousers/{idUser}/todolists/{id}")
    fun deleteList(@Path("idUser") idUser: Int, @Path("id") listId: Int): Call<TaskList>

    @GET("todousers/{idUser}/todolists/{idLlista}/todoitems")
    fun getItemsOfList(@Path("idUser") idUser: Int, @Path("idLlista") listId: Int): Call<MutableList<TaskItem>>

    @POST("todousers/{idUser}/todolists/{idLlista}/todoitems")
    fun addItem(@Path("idUser") idUser: Int, @Path("idLlista") listId: Int, @Body taskItem: TaskItem): Call<TaskItem>

    @DELETE("todousers/{idUser}/todolists/{idLlista}/todoitems/{idItem}")
    fun deleteItem(@Path("idUser") idUser: Int, @Path("idLlista") listId: Int, @Path("idItem") idItem: Int): Call<TaskItem>

    @PUT("todousers/{idUser}/todolists/{idLlista}/todoitems/{idItem}")
    fun checkItem(@Path("idUser") idUser: Int, @Path("idLlista") listId: Int, @Path("idItem") idItem: Int): Call<TaskItem>

    companion object {
        val BASE_URL = "https://taskt-api.herokuapp.com/"

        fun create(): APIService {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(APIService::class.java)
        }
    }

}