package com.example.tasktapp.data.repository

import com.example.tasktapp.data.api.APIService
import com.example.tasktapp.data.model.TaskItem
import com.example.tasktapp.data.model.TaskList
import com.example.tasktapp.data.model.User
import retrofit2.http.Body

class Repository {
    private val apiService = APIService.create()
    fun getUsers() = apiService.getUsers()
    fun register(username: String, password: String) = apiService.register(User(null, username, password, null))
    fun getLists(idU: Int) = apiService.getLists(idU)
    fun addList(idU: Int, taskList: TaskList) = apiService.addList(idU, taskList)
    fun deleteList(idU: Int, id: Int) = apiService.deleteList(idU, id)
    fun getItemsOfList(idU: Int, id: Int) = apiService.getItemsOfList(idU, id)
    fun addItem(idU: Int, idL: Int, taskItem: TaskItem) = apiService.addItem(idU, idL, taskItem)
    fun deleteItem(idU: Int, idL: Int, idI: Int) = apiService.deleteItem(idU, idL, idI)
    fun checkItem(idU: Int, idL: Int, idI: Int) = apiService.checkItem(idU, idL, idI)
}