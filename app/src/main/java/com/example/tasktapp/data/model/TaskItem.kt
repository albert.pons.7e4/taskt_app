package com.example.tasktapp.data.model

import com.google.gson.annotations.SerializedName

data class TaskItem(@SerializedName("idItem") var idItem: Int?, @SerializedName("idLlista") var idLlista: Int, @SerializedName("nomItem") var nomItem: String, @SerializedName("position") var position: Int, @SerializedName("checked") var isChecked: Boolean)