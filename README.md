# TASKT APP DOCUMENTATION
Esta APP ha sido desarrollada por LJA Development, su enfoque es para la APP/WEB TASKT.

Consiste en adaptar nuestra version Taskt web para dispositivos mobiles.

En su versión actual permite almacenar, modificar y eliminar listas e items.

## UTILITY
En la página principal puedes encontrar las listas creadas tanto por la web como la misma app ya que está conectada a nuestra TASKT_api™, Junto a esto, un botón de información y de crear listas. A parte tanto para los ítems como para las listas puedes hacer swipe para eliminarlos. 

Por otra parte, cuando clicas en una lista se abre otra pantalla donde encuentras los items de esta misma, los cuales puedes checkear si estan completados o no.

## UI
TASKT APP dispone de una moderna interficie y animaciones para hacer de la experiencia de usuario más amena.

.
<p align="center">
  <img src="app/src/main/res/drawable/logo.png">
</p>
